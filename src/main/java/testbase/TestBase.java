package testbase;

import java.io.File;
import java.time.Duration;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Listeners;

@Listeners(value = TestListener.class)
public class TestBase {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);
  protected static final Configuration CONFIG = loadConfig();

  protected static final WebDriver driver = buildChromeDriver();

  public static WebDriver buildChromeDriver() {
    new ChromeDriverService.Builder()
        .withLogFile(new File(System.getProperty("user.dir") + "/test-output/ChromeDriver.log"))
        .build();

    LOGGER.info("Building Chrome Driver");
    ChromeOptions options = new ChromeOptions();
    if (CONFIG.getBoolean("isChromeHeadless")) {
      LOGGER.debug("Setting headless Chrome option");
      options.addArguments("--headless=new");
    }
    options.addArguments("--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors",
        "--disable-extensions", "--no-sandbox", "--disable-dev-shm-usage",
        "--remote-allow-origins=*");
    WebDriver driver = new ChromeDriver(options);
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    return driver;
  }

  @AfterMethod
  public void clearSession() {
    driver.manage().deleteAllCookies();
  }

  @AfterTest
  public void tearDown() {
    driver.quit();
  }

  private static PropertiesConfiguration loadConfig() {
    try {
      return new Configurations().properties("config.properties");
    } catch (ConfigurationException e) {
      throw new IllegalStateException("Could not load config.properties");
    }
  }

}
