package testbase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        LOGGER.info("TEST STARTED: {}", result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        LOGGER.info("✅ PASS");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LOGGER.info("❌ FAIL");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        LOGGER.info("❔ SKIP");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        LOGGER.info("❌ FAIL");
    }

    @Override
    public void onStart(ITestContext context) {
        // Unused
    }

    @Override
    public void onFinish(ITestContext context) {
        // Unused
    }

}
