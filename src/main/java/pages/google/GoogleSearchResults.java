package pages.google;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearchResults {

  public static final String URL = "https://www.google.com";
  private WebDriver driver;

  @FindBy(xpath = "//h3[contains(text(),'Selenium')]")
  private WebElement seleniumLink;

  public GoogleSearchResults(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  public SeleniumHomepage clickSeleniumResult() {
    seleniumLink.click();
    return new SeleniumHomepage(driver);
  }
}
