package pages.google;

import java.time.Duration;

import org.awaitility.Awaitility;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleHomepage {

  public static final String URL = "https://www.google.com";
  private WebDriver driver;

  @FindBy(css = "div > textarea")
  private WebElement searchInput;

  @FindBy(css = "form > div > div > div > center > input[value='Google Search']")
  private WebElement searchButton;

  public GoogleHomepage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  public GoogleHomepage loadPage() {
    driver.get(URL);
    return this;
  }

  public GoogleSearchResults searchForSelenium() {
    searchInput.sendKeys("selenium");
    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchButton);
    return new GoogleSearchResults(driver);
  }
}
