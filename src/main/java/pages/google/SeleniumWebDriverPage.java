package pages.google;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SeleniumWebDriverPage {

  @SuppressWarnings("unused")
  private WebDriver driver;

  // WebElement objects go here

  public SeleniumWebDriverPage(WebDriver driver) {
    // Page has reference to WebDriver for acting directly on the browser
    this.driver = driver;
    // WebElement objects initialized here - consider various wait strategies
    PageFactory.initElements(driver, this);
  }

  // WebElement actions go here
}
