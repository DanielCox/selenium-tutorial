package pages.google;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SeleniumHomepage {

  private WebDriver driver;

  @FindBy(xpath = "//a[@href='/documentation/webdriver/']")
  private WebElement seleniumWebDriverReadMoreBtn;

  public SeleniumHomepage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  public SeleniumWebDriverPage clickSeleniumWebDriverReadMore() {
    seleniumWebDriverReadMoreBtn.click();
    return new SeleniumWebDriverPage(driver);
  }
}
