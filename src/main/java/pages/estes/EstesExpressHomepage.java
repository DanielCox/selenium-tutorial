package pages.estes;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EstesExpressHomepage {

	public EstesExpressHomepage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "home-solutions-callout-1")
	private WebElement lessThanTruckload;

	public void clickLessThanTruckload() {
		lessThanTruckload.click();
	}
}
