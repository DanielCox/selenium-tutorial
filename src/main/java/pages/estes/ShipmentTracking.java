package pages.estes;

import org.awaitility.Awaitility;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShipmentTracking {

	public ShipmentTracking(WebDriver driver) {
		PageFactory.initElements(driver, ShipmentTracking.class);
	}

	@FindBy(xpath = "//span[contains(text(),'Shipment Tracking')]")
	private WebElement shipmentTrackingHeader;

	public void waitForHeaderToShow() {
		shipmentTrackingHeader.isDisplayed();
		Awaitility.await().ignoreException(NotFoundException.class).until(() -> shipmentTrackingHeader.isDisplayed());
	}
}
