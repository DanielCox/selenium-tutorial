package learning;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testbase.TestBase;

/**
 * https://www.selenium.dev/documentation/webdriver/elements/locators/
 */
public class LocatorTest extends TestBase {

	private static Logger logger = LoggerFactory.getLogger(LocatorTest.class);

	@BeforeMethod
	public void loadPage() {
		driver.get("https://estes-express-uat.estesinternal.com/");
	}

	@Test
	public void traditionalLocatorsTest() {

		// Partial link text
		WebElement lessThan = driver.findElement(By.partialLinkText("Less-Than"));
		logger.info("Partial link text: " + lessThan.getText());

		// ID
		WebElement trackingInput = driver.findElement(By.id("pro-number"));
		trackingInput.sendKeys("9999999999");
		logger.info("The value we entered was: " + trackingInput.getAttribute("value"));

		// CSS Selectors
		List<WebElement> containerElementsByCssSelector = driver.findElements(By.cssSelector(".container"));
		logger.info("There are still: " + containerElementsByCssSelector.size()
				+ " elements with css selectors '.container'");

		// XPath - Specific use cases like finding by text of an element
		WebElement newToEstes = driver.findElement(By.xpath("//h3[contains(text(),'New to Estes?')]"));
		logger.info(newToEstes.getText());
	}

}
