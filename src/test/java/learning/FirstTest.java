package learning;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testbase.TestBase;

/**
 * https://www.selenium.dev/documentation/webdriver/getting_started/first_script/
 */
public class FirstTest extends TestBase {

	private static Logger logger = LoggerFactory.getLogger(FirstTest.class);

	// Step 1 - Start the session - done in TestNG annotations in the super class

	// Step 2 - Take action on browser - Handle in a TestNG annotation
	@BeforeMethod
	public void loadPage() {
		driver.get("https://estes-express-uat.estesinternal.com/");
	}

	// Step 3 - Request browser information
	@Test
	public void testBrowserInfo() {
		String title = driver.getTitle();
		logger.info(title);
	}

	// Step 4 and 5 - Find an element and Establish Waiting Strategy (See: PageTest)

	// Step 6 - Take action on element
	@Test
	public void testInteract() {
		WebElement trackingInput = driver.findElement(By.id("pro-number"));
		trackingInput.sendKeys("9999999999");
		WebElement trackNowButton = driver.findElement(By.id("home-btn-track-now"));
		trackNowButton.click();
	}

	// Step 7 - Request element information
	@Test
	public void requestExample() {
		WebElement firstParagraph = driver.findElement(By.className("callouts"));
		logger.info(firstParagraph.getText());
	}

	// Step 8 - End the session - Used in the super class' TestNG annotation
}
