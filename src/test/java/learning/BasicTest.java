package learning;

import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;
import pages.google.GoogleHomepage;
import pages.google.GoogleSearchResults;
import pages.google.SeleniumHomepage;
import testbase.TestBase;

public class BasicTest extends TestBase {

  /**
   * Example of traversing web pages by chaining methods
   */
  @Test
  public void testExampleChaining() {
    GoogleHomepage google = new GoogleHomepage(driver);
    google.loadPage().searchForSelenium().clickSeleniumResult().clickSeleniumWebDriverReadMore();
    assertTrue(driver.getTitle().equals("WebDriver | Selenium"));
  }

  /**
   * Identical to the previous, but without chaining. This style is more useful
   * for making multiple
   * operations on a particular page, or for intermediate operations such as
   * logging
   */
  @Test
  public void testExampleLineByLine() {
    GoogleHomepage googleHomepage = new GoogleHomepage(driver);
    googleHomepage.loadPage();
    GoogleSearchResults googleSearchResults = googleHomepage.searchForSelenium();
    SeleniumHomepage seleniumHomepage = googleSearchResults.clickSeleniumResult();
    seleniumHomepage.clickSeleniumWebDriverReadMore();
    assertTrue(driver.getTitle().equals("WebDriver | Selenium"));
  }

}
