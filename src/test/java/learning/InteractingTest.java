package learning;

import java.time.Duration;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import testbase.TestBase;

/**
 * https://www.selenium.dev/documentation/webdriver/elements/interactions/
 * (Click, clear, send keys, submit, select)
 */
public class InteractingTest extends TestBase {

	private static Logger logger = LoggerFactory.getLogger(InteractingTest.class);

	@Test
	public void testInteracting() {
		driver.get("https://estes-express-uat.estesinternal.com/myestes/terminal-lookup/");

		WebElement countryInput = driver.findElement(By.cssSelector("div.mat-select-value"));
		countryInput.click();

		WebElement canadaOption = driver
				.findElement(By.xpath("//span[@class='mat-option-text' and contains(text(),'Canada')]"));
		canadaOption.click();

		WebElement postalCodeInput = driver.findElement(By.id("zip"));
		postalCodeInput.sendKeys("23060");
		postalCodeInput.clear();
		postalCodeInput.sendKeys("M4V");

		By postalCodeBy = By.xpath("//span[contains(text(),'M4V0A1')]");
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOfElementLocated(postalCodeBy));
		WebElement postalCodeOption = driver.findElement(postalCodeBy);
		postalCodeOption.click();

		postalCodeInput.submit();
	}

	@Test
	public void testSelect() {
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select");
		driver.switchTo().frame("iframeResult");
		Select select = new Select(driver.findElement(By.cssSelector("select#cars")));
		select.selectByIndex(1);
		select.selectByVisibleText("Opel");
		String options = select.getOptions().stream().map(WebElement::getText).collect(Collectors.joining(", "));
		logger.info(options);

	}
}
