package learning;

import java.time.Duration;
import org.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.estes.EstesExpressHomepage;
import testbase.TestBase;

/**
 * https://www.selenium.dev/documentation/test_practices/encouraged/page_object_models/
 */
public class PageTest extends TestBase {

	private static Logger logger = LoggerFactory.getLogger(PageTest.class);

	@BeforeMethod
	public void loadPage() {
		driver.get("https://estes-express-uat.estesinternal.com/");
		driver.manage().window().maximize();
	}

	@Test
	public void testClickLessThanTruckload() {
		logger.info("Click less than truckload");
		EstesExpressHomepage page = new EstesExpressHomepage(driver);
		page.clickLessThanTruckload();

		String ltlTitle = "LTL Freight Shipping Company | Less-Than-Truckload Carrier | Estes";
		Awaitility.await().atMost(Duration.ofSeconds(10)).until(() -> driver.getTitle().equals(ltlTitle));
	}
}
