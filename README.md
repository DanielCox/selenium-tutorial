# Selenium Tutorial

This project supplements this [Automation KT](https://docs.google.com/presentation/d/1VvhziR_0UZ4PVTGaXiMi98-CJmXiFsWnf485WCUxt2c/edit?usp=sharing), this is a collection of Selenium examples used in conjunction with TestNG.

## Note
For a more complete project with automation support for iSeries and Android, and reporting, refer to the [Automation Template](https://gitlab.com/DanielCox/automation-template). 
